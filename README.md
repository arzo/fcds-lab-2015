# Foundations of Concurrent and Distributed Systems Lab: Summer semester 2015 #

This repository contains 5 programming tasks, with their descriptions, sequential C sources, and test inputs. The tasks are taken from the 7th Marathon of Parallel Programming WSCAD-SSC/SBAC-PAD-2012.

# Contributors #

Dmitrii Kuvaiskii <dmitrii.kuvaiskii@tu-dresden.de>

# List of Participants

## First defending group (17:00 - 18:30)

* Antonio Monteiro -- Go

## Second defending group (18:30 - 20:00)

* Rouchun Tzeng -- C

* Miquel Palacios -- Java

* Peter Heisig -- Java

## Undecided

* Christof Leonhardt -- Java (Fork/Join)

* Maya Shallouf -- Erlang

* Martin Rataj -- C++ & Rust

* Jörg Thalheim -- Go

* Michael Ullrich -- C

* Javid Abbasov -- Go

* Florian Blume -- C++

* Paul Winter -- C++

* Leonardo Marques -- C

* Xing Xiaosha -- Java

* Shen Tong -- Java

* Johannes Karl -- C

* Antonio Coelho -- Python

* Donghao Lu -- Java

* Artur Zochniak -- Erlang