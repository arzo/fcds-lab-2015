#include <stdlib.h>
#include <string.h>
#include "bucketsort.h"
#include <omp.h>

#define N_BUCKETS 94

typedef struct {
	long int *data;
	int length;
	long int total;
} bucket;

void sort(char *a, bucket *bucket) {
	int j, i, length;
	long int key;
	length = bucket->length;
	for (j = 1; j < bucket->total; j++) {
		key = bucket->data[j];
		i = j - 1;
		while (i >= 0
				&& strcmp(a + bucket->data[i] * length, a + key * length) > 0) {
			bucket->data[i + 1] = bucket->data[i];
			i--;
		}
		bucket->data[i + 1] = key;
	}
}

long int* bucket_sort(char *a, int length, long int size, int cores) {

	long int i;
	bucket buckets[N_BUCKETS], *b;
	long int *returns;

	// allocate memory
	returns = malloc(sizeof(long int) * size);
	for (i = 0; i < N_BUCKETS; i++) {
		buckets[i].data = returns + i * size / N_BUCKETS;
		buckets[i].length = length;
		buckets[i].total = 0;
	}

    omp_set_num_threads(cores);
    int max = omp_get_max_threads();
    int chunksize = size/max;
    if(chunksize<1) chunksize=1;
    int id;
    printf("maxthreads=%d chunk size=%d cores=%d\n", max, chunksize);
    #pragma omp parallel default(none) private(i, id)\
    shared(size,chunksize,b,buckets,a,length)
    {
        id = omp_get_thread_num();
        #pragma omp for schedule(static,chunksize)
        for (i = 0; i < size; i++) {
            b = &buckets[*(a + i * length) - 0x21];
            b->data[b->total++] = i;
        }
    }

    #pragma omp barrier
    #pragma omp parallel default(none) private(i, id) \
    shared(buckets,a,chunksize)
    {
        id = omp_get_thread_num();
        #pragma omp for schedule(static)
        for (i = 0; i < N_BUCKETS; i++) {
            printf("bucket %d: handled by th_id %d aka %d/%d\n", i, id, omp_get_thread_num()+1, omp_get_num_threads());
            sort(a, &buckets[i]);
        }
    }
	return returns;
}
