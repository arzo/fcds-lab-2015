#include <stdio.h>
#include <stdlib.h>
#include "bucketsort.h"
#include "../stoper.h"

#define LENGTH 8

FILE *fin, *fout;

char *strings;
long int N;

void openfiles(char* in, char* out) {
	fin = fopen(in, "r+");
	if (fin == NULL) {
		perror("fopen fin");
		exit(EXIT_FAILURE);
	}

	fout = fopen(out, "w");
	if (fout == NULL) {
		perror("fopen fout");
		exit(EXIT_FAILURE);
	}
}

void closefiles(void) {
	fclose(fin);
	fclose(fout);
}

#include <omp.h>

int main2(int argc, char *argv[]) {
    {
        int nthreads,tid;
        #pragma omp parallel private(nthreads, tid)
        {
            tid = omp_get_thread_num();
            printf("Hello World from thread = %d\n", tid);

            if (tid == 0) {
                nthreads = omp_get_num_threads();
                printf("Only in master: Number of threads = %d\n", nthreads);
            }
        }
    }
}

int main(int argc, char* argv[]) {
    main2(argc, argv);
	long int i, *r;

    if (argc < 3) {
        printf("usage: %s input_file output_file [cores=1] [reportfile=report.csv]\n", argv[0]);
        return 0;
    }

	openfiles(argv[1], argv[2]);

	int cores = 1;
	if(argc>=4) sscanf(argv[3], "%d", &cores);
	printf("using that many cores: %d\n", cores);

	fscanf(fin, "%ld", &N);
	strings = (char*) malloc(N * LENGTH);
	if (strings == NULL) {
		perror("malloc strings");
		exit(EXIT_FAILURE);
	}

	for (i = 0; i < N; i++)
		fscanf(fin, "%s", strings + (i * LENGTH));

	fflush(stdout);

    timeStart(argv[1], cores);
    r = bucket_sort(strings, LENGTH, N, cores);
	timeEnd(argc>=5?argv[5-1]:"report.csv");

	fflush(stdout);

	for (i = 0; i < N; i++) {
		fprintf(fout, "%s\n", strings + (r[i] * LENGTH));
    }

	free(r);
	free(strings);
	closefiles();

	return EXIT_SUCCESS;
}
