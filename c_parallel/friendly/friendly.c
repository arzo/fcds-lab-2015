#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "../stoper.h"
#include <omp.h>

int gcd(int u, int v) {
	if (v == 0)
		return u;
	return gcd(v, u % v);
}

void friendly_numbers(long int start, long int end, int cores) {
	long int last = end - start + 1;

	long int *the_num;
	the_num = (long int*) malloc(sizeof(long int) * last);
	long int *num;
	num = (long int*) malloc(sizeof(long int) * last);
	long int *den;
	den = (long int*) malloc(sizeof(long int) * last);
	long int *fromThread;
	fromThread = (long int*) malloc(sizeof(long int) * last);

	long int i, j, chunksize;

    int id;
    omp_set_num_threads(cores);
    chunksize=(end-start)/omp_get_max_threads();
    if(chunksize<1) chunksize=1;
    #pragma omp parallel default(none) private(i, id) \
    shared(fromThread,start,end,den,num,the_num,chunksize)
    {
    //printf("hello from thread %d\n", omp_get_thread_num());
    #pragma omp for schedule(static, chunksize)
	for (i = start; i <= end; i++) {
		long int sum, done, factor, n;
		long int ii = i - start;
		sum = 1 + i;
		the_num[ii] = i;
		done = i;
		factor = 2;
		while (factor < done) {
			if ((i % factor) == 0) {
				sum += (factor + (i / factor));
				if ((done = i / factor) == factor)
					sum -= factor;
			}
			factor++;
		}
		num[ii] = sum;
		den[ii] = i;
		n = gcd(num[ii], den[ii]);
		num[ii] /= n;
		den[ii] /= n;
		fromThread[ii]=omp_get_thread_num();
	} // end for
    }

    #pragma omp barrier
    // printf("world from thread %d\n", omp_get_thread_num());

	for (i = 0; i < last; i++) {
		for (j = i + 1; j < last; j++) {
			if ((num[i] == num[j]) && (den[i] == den[j]))
				// printf("%ld and %ld are FRIENDLY (from thread %d and %d)\n", the_num[i], the_num[j], fromThread[i], fromThread[j]);
				printf("%ld and %ld are FRIENDLY\n", the_num[i], the_num[j]);
		}
	}

	free(fromThread);
	free(the_num);
	free(num);
	free(den);
}

int main(int argc, char **argv) {
	long int start;
	long int end;

	if(argc<2) {perror("usage: prog label [cores=1] [reportfname=report.csv]");return 1;}

    int cores=argc>=3?atoi(argv[2]):1;
    timeStart(argv[1], cores);
	while (1) {
		scanf("%ld %ld", &start, &end);
		if (start == 0 && end == 0)
			break;
		printf("Number %ld to %ld\n", start, end);
		friendly_numbers(start, end, cores);
	}
	timeEnd(argc>=4?argv[4-1]:"report.csv");

	return EXIT_SUCCESS;
}
