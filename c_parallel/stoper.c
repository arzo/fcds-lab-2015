#include <stdio.h>
#include <time.h>
#include "stoper.h"
#include <stdlib.h>
#include <string.h> //strerror(err)

#include <time.h>

clock_t start;
clock_t end;
time_t tstart,tend;
float cputimeInSeconds, userTimeInSec;
const char *fname;
int cores;
time_t mytime;

void timeStart(const char *_fname, int _cores) {
    fname=_fname;
    cores=_cores;
    mytime = time(NULL);
    time(&tstart);
    start = clock();

}

void timeEnd(const char *reportfname) {
    end=clock();
    time(&tend);
    cputimeInSeconds = (float)(end - start) / CLOCKS_PER_SEC;
    userTimeInSec=difftime(tend,tstart);

	FILE *fout = fopen(reportfname, "a");
	if (fout == NULL) {
		perror("fopen fout for report");
		exit(EXIT_FAILURE);
	}

	fprintf(fout, "\"%s\"; %d; %f; %f; %s", fname, cores,
        userTimeInSec, cputimeInSeconds, ctime(&mytime));
	fclose(fout);
}
