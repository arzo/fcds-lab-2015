#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "../stoper.h"
#include <omp.h>

// Solves the 3-SAT system using an exaustive search
// It finds all the possible values for the set of variables using
// a number. The binary representation of this number represents
// the values of the variables. Since a long variable has 64 bits,
// this implementations works with problems with up to 64 variables.
long solveClauses(short **clauses, int nClauses, int nVar, int cores) {

	long *iVar = (long *) malloc(nVar * sizeof(long));
	int i;
	for (i = 0; i < nVar; i++)
		iVar[i] = exp2(i);

	unsigned long maxNumber = exp2(nVar);
	long number;
	short var;
	int c;

    omp_set_num_threads(cores);
    unsigned long int chunksize=maxNumber/omp_get_max_threads();
    if(chunksize<1) chunksize=1;
    unsigned long int id=0;
    unsigned long int winner=0, num_win=0, ccc;
    #pragma omp parallel default(none) private(number, id, c, ccc, var) \
    shared(winner,num_win,nClauses,clauses,maxNumber,iVar,chunksize)
    {
    // id = omp_get_thread_num();
    // printf("thread id =%d, chunksize=%ld maxnr=%ld\n", id, chunksize, maxNumber);
    #pragma omp for schedule(static, chunksize)
	for (number = 0; number < maxNumber; number++) {
		for (c = 0; c < nClauses; c++) {

			var = clauses[0][c];
			if (var > 0 && (number & iVar[var - 1]) > 0)
				continue; // clause is true
			else if (var < 0 && (number & iVar[-var - 1]) == 0)
				continue; // clause is true

			var = clauses[1][c];
			if (var > 0 && (number & iVar[var - 1]) > 0)
				continue; // clause is true
			else if (var < 0 && (number & iVar[-var - 1]) == 0)
				continue; // clause is true

			var = clauses[2][c];
			if (var > 0 && (number & iVar[var - 1]) > 0)
				continue; // clause is true
			else if (var < 0 && (number & iVar[-var - 1]) == 0)
				continue; // clause is true

			break; // clause is false
		}

		if (c == nClauses) {
			winner=1;
			num_win=number;
        }

	}
	}
	if(winner>0) return num_win;
	return -1;

}

// Read nClauses clauses of size 3. nVar represents the number of variables
// Clause[0][i], Clause[1][i] and Clause[2][i] contains the 3 elements of the i-esime clause.
// Each element of the caluse vector may contain values selected from:
// k = -nVar, ..., -2, -1, 1, 2, ..., nVar. The value of k represents the index of the variable.
// A negative value remains the negation of the variable.
short **readClauses(int nClauses, int nVar) {

	short **clauses = (short **) malloc(3 * sizeof(short *));
	clauses[0] = (short *) malloc(nClauses * sizeof(short));
	clauses[1] = (short *) malloc(nClauses * sizeof(short));
	clauses[2] = (short *) malloc(nClauses * sizeof(short));

	int i;
	for (i = 0; i < nClauses; i++) {
		scanf("%hd %hd %hd", &clauses[0][i], &clauses[1][i], &clauses[2][i]);
	}

	return clauses;
}

int main(int argc, char *argv[]) {

	int nClauses;
	int nVar;

	scanf("%d %d", &nClauses, &nVar);

	short **clauses = readClauses(nClauses, nVar);

    int cores=argc>=3?atoi(argv[3-1]):1;

    timeStart(argv[1], cores);
	long solution = solveClauses(clauses, nClauses, nVar, cores);
	timeEnd(argc>=4?argv[4-1]:"report.csv");

    free(clauses[0]);
    free(clauses[1]);
    free(clauses[2]);
    free(clauses);

	int i;
	if (solution >= 0) {
		printf("Solution found [%ld]: ", solution);
		for (i = 0; i < nVar; i++)
			printf("%d ", (int) ((solution & (long) exp2(i)) / exp2(i)));
		printf("\n");
	} else
		printf("Solution not found.\n");

	return EXIT_SUCCESS;
}

