#ifndef STOPER_H_INCLUDED
#define STOPER_H_INCLUDED

void timeStart(const char *fname, int cores);
void timeEnd(const char *reportfname);

#endif // STOPER_H_INCLUDED
